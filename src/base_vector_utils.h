#pragma once

#include <Eigen/Core>


double distance(Eigen::Vector3d lhs, Eigen::Vector3d rhs)
{
    Eigen::Vector3d tmp = lhs - rhs;
    return sqrt(tmp.dot(tmp));
}