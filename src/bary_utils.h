#pragma once

#include <Eigen/Core>


Eigen::Vector3d bary_to_cart(const Eigen::Vector3d& barycentric, const Eigen::MatrixXd& V, const Eigen::MatrixXi& F, int fid)
{
    Eigen::Vector3i vertices_ids = F.row(fid);
    Eigen::Matrix3d vertices;
    vertices << V.row(vertices_ids[0]), V.row(vertices_ids[1]), V.row(vertices_ids[2]);
    vertices.transposeInPlace();
    Eigen::Vector3d cart = vertices * barycentric;
    return cart;
}