#pragma once

#include <Eigen/Core>


struct VF
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
};

struct VFC
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    Eigen::MatrixXd C;
};

using JointsList = std::vector<VF>;
