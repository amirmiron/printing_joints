#pragma once

#include <Eigen/Core>

void append_matrices(Eigen::MatrixXd& V, Eigen::MatrixXi& F, Eigen::MatrixXd& C, const Eigen::MatrixXd& add_V, const Eigen::MatrixXi& add_F, const Eigen::MatrixXd& add_C)
{
    assert(V.cols() == add_V.cols());
    assert(F.cols() == add_F.cols());
    assert(C.cols() == add_C.cols());
    assert(F.rows() == C.rows());
    assert(add_F.rows() == add_C.rows());
    uint64_t old_V_rows = V.rows();
    V.conservativeResize(V.rows()+add_V.rows(), V.cols());
    V.block(old_V_rows, 0, add_V.rows(), add_V.cols()) << add_V;
    uint64_t old_F_rows = F.rows();
    F.conservativeResize(F.rows() + add_F.rows(), F.cols());
    F.block(old_F_rows, 0, add_F.rows(), F.cols()) << (add_F + Eigen::MatrixXi::Constant(add_F.rows(), add_F.cols(), old_V_rows));
    uint64_t old_C_rows = C.rows();
    C.conservativeResize(C.rows() + add_C.rows(), C.cols());
    C.block(old_C_rows, 0, add_C.rows(), add_C.cols()) << add_C;
}
