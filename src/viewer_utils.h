#pragma once

#include <Eigen/Core>
#include <igl/opengl/glfw/Viewer.h>
#include "defs.h"

Eigen::Vector3d RED()
{
    return Eigen::Vector3d(1, 0, 0);
}
Eigen::Vector3d GREEN()
{
    return Eigen::Vector3d(0, 1, 0);
}
Eigen::Vector3d BLUE()
{
    return Eigen::Vector3d(0, 0, 1);
}

void recolor_viewer_data(const VF& model, const JointsList& joints, uint64_t selected_joint, VFC& viewer_vfc, igl::opengl::glfw::Viewer& viewer)
{
    uint64_t joints_F_rows = 0;
    for (auto joint : joints)
    {
        joints_F_rows += joint.F.rows();
    }

    Eigen::MatrixXd base_joint_color = GREEN().transpose().replicate(joints_F_rows, 1);
    viewer_vfc.C << Eigen::MatrixXd::Constant(model.F.rows(), 3, 1.0), base_joint_color;
    uint64_t selected_joint_start_row = 0;
    assert(selected_joint <= joints.size());
    for (uint64_t i = 0; i < selected_joint; i++)
    {
        selected_joint_start_row += joints[i].F.rows();
    }
    if (!joints.empty())
    {
        Eigen::MatrixXd red_block = RED().transpose().replicate(joints[selected_joint].F.rows(), 1);
        viewer_vfc.C.block(model.F.rows() + selected_joint_start_row, 0, red_block.rows(), viewer_vfc.C.cols()) << red_block;
    }
    viewer.data().set_colors(viewer_vfc.C);
}

void full_update_viewer_data(const VF& model, const JointsList& joints, uint64_t selected_joint, VFC& viewer_vfc, igl::opengl::glfw::Viewer& viewer)
{
    uint64_t joints_V_rows = 0;
    uint64_t joints_F_rows = 0;
    for (auto joint : joints)
    {
        joints_V_rows += joint.V.rows();
        joints_F_rows += joint.F.rows();
    }
    viewer_vfc.V.resize(model.V.rows() + joints_V_rows, model.V.cols());
    viewer_vfc.V.block(0, 0, model.V.rows(), model.V.cols()) << model.V;
    viewer_vfc.F.resize(model.F.rows() + joints_F_rows, model.F.cols());
    viewer_vfc.F.block(0, 0, model.F.rows(), model.F.cols()) << model.F;
    uint64_t cur_V_row = model.V.rows();
    uint64_t cur_F_row = model.F.rows();
    for (auto joint : joints)
    {
        viewer_vfc.V.block(cur_V_row, 0, joint.V.rows(), joint.V.cols()) << joint.V;
        viewer_vfc.F.block(cur_F_row, 0, joint.F.rows(), joint.F.cols()) << (joint.F + Eigen::MatrixXi::Constant(joint.F.rows(), joint.F.cols(), cur_V_row));
        cur_V_row += joint.V.rows();
        cur_F_row += joint.F.rows();
    }
    viewer_vfc.C.resize(viewer_vfc.F.rows(), viewer_vfc.F.cols());
    viewer.data().clear();

    viewer.data().set_mesh(viewer_vfc.V, viewer_vfc.F);
    recolor_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);

}
