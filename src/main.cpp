#include <iostream>
#include <igl/opengl/glfw/Viewer.h>
//#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
//#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
//#include <imgui/imgui.h>
#include <igl/unproject_onto_mesh.h>
#include <igl/ray_mesh_intersect.h>
#include "point_spheres.h"
#include "bary_utils.h"
#include "base_matrix_utils.h"
#include "base_vector_utils.h"
#include "defs.h"
#include "viewer_utils.h"

const uint64_t SPHERE_RESOLUTION = 20;
const double SPHERE_RADIUS = 5.0;


VF model;

JointsList joints;
uint64_t selected_joint;

VFC viewer_vfc;


void point_sphere(Eigen::Vector3d& center, double radius, uint64_t res, Eigen::MatrixXd& V, Eigen::MatrixXi& F)
{
    Eigen::MatrixXd spheres_centers(1, 3);
    spheres_centers << center.transpose();
    point_spheres(spheres_centers, radius, res, V, F);
}

uint64_t add_joint(Eigen::Vector3d& center, double radius, uint64_t res)
{
    uint64_t joint_id = joints.size();
    joints.emplace_back();
    auto new_joint = &joints[joints.size() - 1];
    point_sphere(center, radius, res, new_joint->V, new_joint->F);
    return joint_id;
}

void selected_joint_set(uint64_t i)
{
    assert(i < joints.size());
    selected_joint = i;
}
void selected_joint_next()
{
    if (!joints.empty())
    {
        selected_joint = (selected_joint + 1) % joints.size();
    }
}
void selected_joint_prev()
{
    if (!joints.empty())
    {
        selected_joint = (selected_joint - 1) % joints.size();
    }
}

bool key_pressed(igl::opengl::glfw::Viewer& viewer, unsigned char key, int modifier)
{
    if (key == 'm') {
        int fid;
        Eigen::Vector3d bc;
        // Cast a ray in the view direction starting from the mouse position
        double x = viewer.current_mouse_x;
        double y = viewer.core().viewport(3) - viewer.current_mouse_y;
        std::cout << "x=" << x << " y=" << y << std::endl;
        if (igl::unproject_onto_mesh(Eigen::Vector2f(x, y), viewer.core().view,
            viewer.core().proj, viewer.core().viewport, model.V, model.F, fid, bc))
        {
            Eigen::Vector2f pos(x, y);
            Eigen::Vector3d win_s(pos(0, 0), pos(1, 0), 0);
            // Source, destination and direction in world
            Eigen::Vector3d s;
            igl::unproject(win_s, viewer.core().view, viewer.core().proj, viewer.core().viewport, s);
            std::cout << "s=" << std::endl << s << std::endl;

            Eigen::Vector3d hit_cart = bary_to_cart(bc, model.V, model.F, fid);
            Eigen::Vector3d hit_direction = hit_cart - s;
            std::vector<igl::Hit> rest_of_hits;
            bool has_another_hit = igl::ray_mesh_intersect(s, hit_direction, model.V, model.F, rest_of_hits);
            assert(rest_of_hits.size() >= 2); // shape is 3d, must have another hit
            Eigen::Vector3d next_hit_cart = s + static_cast<double>(rest_of_hits[1].t) * hit_direction;
            Eigen::Vector3d middle_hit_cart = (hit_cart + next_hit_cart) / 2;
            double radius = distance(hit_cart, middle_hit_cart) * 1.1;
            uint64_t new_joint_id = add_joint(middle_hit_cart, radius, SPHERE_RESOLUTION);
            selected_joint = new_joint_id;
            std::cout << "hit: " << hit_cart << std::endl;
            full_update_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);
        }
        return true;
    } else if (key == 'n') {
        selected_joint_next();
        recolor_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);
        return true;
    } else if (key == 'b')
    {
        selected_joint_prev();
        recolor_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);
        return true;
    }
    else if (key == 'v')
    {
        if (!joints.empty())
        {
            joints.erase(joints.begin() + selected_joint);
            selected_joint_next();
            full_update_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);
        }
        return true;
    }
    return false;
}

int main(int argc, char *argv[])
{
    // Load a mesh in OFF format
    std::ifstream file_stream("D:/uni/3d_printing/project_joints/possible_models/Angelic_Valla_-_Demon_Hunter_/files/Valla3.0.stl", std::ios::binary);
    {
        Eigen::MatrixXd model_N; // nothing uses the normals...
        igl::readSTL(file_stream, model.V, model.F, model_N);
    }

    // Plot the mesh
    igl::opengl::glfw::Viewer viewer;
    std::cout << "Usage: \n"
        "  [m]  Pick joint location\n"
        "  [n]  Select next joint\n"
        "  [b]  Select previous joint\n"
        "  [v]  Remove selected joint\n"
        "\n";


    // Attach a menu plugin
    //igl::opengl::glfw::imgui::ImGuiMenu menu;
    //viewer.plugins.push_back(&menu);

    viewer.callback_key_pressed = &key_pressed;
    full_update_viewer_data(model, joints, selected_joint, viewer_vfc, viewer);

    viewer.launch();
}
